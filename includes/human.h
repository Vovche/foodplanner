#ifndef HUMAN_H
#define HUMAN_H

#include "demo.h"
#include <functional>

namespace human_stuff
{
    enum class Harrben_activity_coeffs : size_t {
        minimum = 0, low, middle, middleplus, high, highplus, godmode
    };
    
    constexpr std::array<double, 7> hbactivity = {
        1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8
    };

    constexpr std::array<std::array<double, 4>, 2> hbcoeffs =
    {{
        {88.362, 13.397, 4.799, 5.677}, //man
        {447.593, 9.247, 3.098, 4.330} //not man
    }}; 

    enum class Sex {male, female};

    class Human
    {
        using HAC = Harrben_activity_coeffs;
        using func = std::function<double(uint, double, double)>;

    public:
        Human(Sex sex, uint age, double height, double weight,
            HAC coeff = HAC::minimum);

    double get_daily_norm(void);

    private:
        Sex sex_;
        uint age_;
        double height_;
        double weight_;
        func calculate_norm_;
    };
}
#endif
