#ifndef EAT_ITEM
#define EAT_ITEM

#include <iostream>
#include <memory>
#include <unordered_map>
#include <vector>

namespace food_item
{
    using string = std::string;
    class FoodItem
    { 

    public:
        FoodItem(uint id, double calories, double proteins, double fats,
            double carbohydrates, string name = "", double grams = 1);

    public:
        double get_calories(double grams = 1) const;

    //declaring getters with macro
    #define MAKE_GET(TYPE, FIELD)\
    TYPE get_##FIELD(void) const;
        MAKE_GET(uint, id)
        MAKE_GET(string, name)
        MAKE_GET(double, proteins_per_gram)
        MAKE_GET(double, fats_per_gram)
        MAKE_GET(double, carbohydrates_per_gram)
        MAKE_GET(double, calories_per_gram)
    #undef MAKE_GET

    public:
        FoodItem(const FoodItem& rhs);
        FoodItem& operator=(FoodItem& rhs);
        FoodItem(FoodItem&& rhs);
        FoodItem& operator=(FoodItem&& rhs);
        ~FoodItem();

    protected:
        struct Impl;
        std::unique_ptr<Impl> pImpl;

        friend void print_item(const FoodItem &item);
    };

    class ComplexFoodItem : public FoodItem
    {
    public:
        ComplexFoodItem(uint id, string name);
        ComplexFoodItem& add_item(const FoodItem& item, double grams = 1);
    };

    void print_item(const FoodItem &item);
}

#endif
