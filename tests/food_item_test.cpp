#include "gtest/gtest.h"
#include "food_item.h"
#include <cmath>
#include <memory>

using food_item::FoodItem;
using food_item::ComplexFoodItem;

class FoodItemTest : public ::testing::Test
{
protected:
    void SetUp()
    {
        apple = std::make_unique<FoodItem>(0, 1, 1, 1, 1, "apple");
    }
   std::unique_ptr<FoodItem> apple; 
};

TEST_F(FoodItemTest, CaloriesCount)
{
    ASSERT_EQ(apple->get_calories(), 1);
    ASSERT_EQ(apple->get_calories(2), 2);
}

TEST_F(FoodItemTest, Getters)
{
    ASSERT_EQ(apple->get_id(), 0);
    ASSERT_EQ(apple->get_calories_per_gram(), 1);
    ASSERT_EQ(apple->get_proteins_per_gram(), 1);
    ASSERT_EQ(apple->get_fats_per_gram(), 1);
    ASSERT_EQ(apple->get_carbohydrates_per_gram(), 1);
    ASSERT_EQ(apple->get_name(), "apple");
}

TEST_F(FoodItemTest, DivisionWhenInit)
{
    FoodItem apple1(0, 2, 1, 1, 1, "apple", 2);
    ASSERT_EQ(apple->get_calories(), 1);
    ASSERT_EQ(apple1.get_calories(), 1);
}

TEST_F(FoodItemTest, ComplexFoodItemTest)
{
    ComplexFoodItem salad(5, "apple stuff");
    salad.add_item(*apple);
#define DUMMY_TEST(FIELD, VALUE)\
ASSERT_EQ(salad.get_##FIELD##_per_gram(), VALUE);
    DUMMY_TEST(proteins, 1);
    DUMMY_TEST(fats, 1);
    DUMMY_TEST(carbohydrates, 1);
    DUMMY_TEST(calories, 1);
    salad.add_item(*apple, 4);
    DUMMY_TEST(proteins, 5);
    DUMMY_TEST(fats, 5);
    DUMMY_TEST(carbohydrates, 5);
    DUMMY_TEST(calories, 5);
#undef DUMMY_TEST
}
