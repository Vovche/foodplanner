#include "gtest/gtest.h"
#include "human.h"
#include <cmath>

using human_stuff::Human;

TEST(HumanDailyNormTest, Any_sex)
{
    Human female(human_stuff::Sex::female, 30, 165, 65);
    ASSERT_EQ(ceil(female.get_daily_norm()), 1716);
    Human male(human_stuff::Sex::male, 30, 175, 75);
    ASSERT_EQ(ceil(male.get_daily_norm()), 2116);
}

TEST(HumanDailyNormTest, Different_coeffs)
{
    using coeff = human_stuff::Harrben_activity_coeffs;
    std::array<Human, 6> humans = {{
        {human_stuff::Sex::male, 30, 175, 75, coeff::low},
        {human_stuff::Sex::male, 30, 175, 75, coeff::middle},
        {human_stuff::Sex::male, 30, 175, 75, coeff::middleplus},
        {human_stuff::Sex::male, 30, 175, 75, coeff::high},
        {human_stuff::Sex::male, 30, 175, 75, coeff::highplus},
        {human_stuff::Sex::male, 30, 175, 75, coeff::godmode}
    }};
    ASSERT_EQ(ceil(humans[0].get_daily_norm()), 2292);
    ASSERT_EQ(ceil(humans[1].get_daily_norm()), 2468);
    ASSERT_EQ(ceil(humans[2].get_daily_norm()), 2644);
    ASSERT_EQ(ceil(humans[3].get_daily_norm()), 2821);
    ASSERT_EQ(ceil(humans[4].get_daily_norm()), 2997);
    ASSERT_EQ(ceil(humans[5].get_daily_norm()), 3173);
}
