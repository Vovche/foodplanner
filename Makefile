.PHONY: all clean 

NAME = demo

CC = g++

CFLAGS = -g -Wall -Wextra -Werror -I$(INC_DIR) 
TEST_CFLAGS = $(CFLAGS) -lgtest_main -lgtest

INC_DIR = includes/
SRC_DIR = src/
BIN_DIR = bin/
TESTS_DIR = tests/

OBJ_DIR = $(BIN_DIR)obj/
TESTS_ODIR = $(BIN_DIR)tests/

SOURCES = $(shell ls $(SRC_DIR) | grep -v tests)
TESTS_S = $(shell ls $(TESTS_DIR))

OBJS = $(addprefix $(OBJ_DIR), $(SOURCES:.cpp=.o))
TEST_OBJS = $(addprefix $(TESTS_ODIR), $(TESTS_S:.cpp=.o))

TESTS_BIN = $(BIN_DIR)test
TESTS_PASSED_FLAG = $(TESTS_ODIR)passed

STATIC_CHECK = cppcheck -I$(INC_DIR)\
--enable=warning\
--enable=style\
--enable=performance\
--enable=portability\
--enable=information\
--enable=missingInclude\
--suppress=missingIncludeSystem

DELIH = =========== running demo =============
DELIM = ======================================

all: $(BIN_DIR)$(NAME)

run: $(BIN_DIR)$(NAME)
	@echo $(DELIH) 
	@./$(BIN_DIR)$(NAME)
	@echo $(DELIM) 

$(BIN_DIR)$(NAME): $(TESTS_PASSED_FLAG)
	@echo -n Building $@ :
	@$(CC) $(CFLAGS) -o $(BIN_DIR)$(NAME) $(OBJS)
	@echo \ success

$(TESTS_PASSED_FLAG): $(OBJS) $(TEST_OBJS)
	@$(CC) $(TEST_CFLAGS) -o $(TESTS_BIN) $(TEST_OBJS) $(filter-out $(OBJ_DIR)main.o, $(OBJS))
	@./$(TESTS_BIN)
	@touch $(TESTS_PASSED_FLAG)
	@echo

$(TESTS_ODIR)%.o : $(TESTS_DIR)%.cpp
	@rm -f $(TESTS_PASSED_FLAG)
	@mkdir -p $(TESTS_ODIR)
	@echo -n Building $@ :
	@$(CC) $(CFLAGS) -c -o $@ $<
	@echo \ success

$(OBJ_DIR)%.o : $(SRC_DIR)%.cpp 
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(OBJ_DIR)
	@$(STATIC_CHECK) $<
	@echo -n Building $@ :
	@$(CC) $(CFLAGS) -c -o $@ $<
	@echo \ success

clean:
	@rm -rf $(OBJ_DIR) $(NAME) $(BIN_DIR)
