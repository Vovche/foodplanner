#include "food_item.h"

using food_item::FoodItem;
using food_item::ComplexFoodItem;

struct FoodItem::Impl
{
    Impl(uint _id, double _calories, double _proteins, double _fats,
        double _carbohydrates, string _name = "", double _grams = 1) :
            id(_id), name(_name),
            proteins_per_gram(_proteins / _grams),
            fats_per_gram(_fats / _grams),
            carbohydrates_per_gram(_carbohydrates / _grams),
            calories_per_gram(_calories / _grams)
    {

    }
    
    uint id{0};
    string name{""};
    double proteins_per_gram{0};
    double fats_per_gram{0};
    double carbohydrates_per_gram{0};
    double calories_per_gram{0};
};

//defining getters with macro
#define MAKE_GET(TYPE, FIELD)\
TYPE FoodItem::get_##FIELD(void) const\
{return pImpl->FIELD;}
    MAKE_GET(uint, id)
    MAKE_GET(std::string, name)
    MAKE_GET(double, proteins_per_gram)
    MAKE_GET(double, fats_per_gram)
    MAKE_GET(double, carbohydrates_per_gram)
    MAKE_GET(double, calories_per_gram)
#undef MAKE_GET

FoodItem::FoodItem(uint id, double calories, double proteins, double fats,
    double carbohydrates, string name, double grams) :
        pImpl(std::make_unique<Impl>
            (id, calories, proteins, fats, carbohydrates, name, grams))
{

}

double FoodItem::get_calories(double grams) const
{
    return pImpl->calories_per_gram * grams;
}


FoodItem::FoodItem(FoodItem&& rhs) = default;
FoodItem& FoodItem::operator=(FoodItem&& rhs) = default;
FoodItem::~FoodItem() = default;
FoodItem::FoodItem(const FoodItem& rhs)
{
    if (rhs.pImpl) pImpl = std::make_unique<Impl>(*rhs.pImpl);
}

FoodItem& FoodItem::operator=(FoodItem& rhs)
{
    if (!rhs.pImpl) pImpl.reset();
    else if (!pImpl) pImpl = std::make_unique<Impl>(*rhs.pImpl); 
    else *pImpl = *rhs.pImpl;
    return *this;
}

ComplexFoodItem::ComplexFoodItem(uint id, string name) :
    FoodItem(id, 0, 0, 0, 0, name, 1)
{
        
}

ComplexFoodItem& ComplexFoodItem::add_item(const FoodItem& item, double grams)
{
#define ADD_FIELDS(X)\
pImpl->X += item.get_##X() * grams;
    ADD_FIELDS(proteins_per_gram); 
    ADD_FIELDS(fats_per_gram); 
    ADD_FIELDS(carbohydrates_per_gram); 
    ADD_FIELDS(calories_per_gram); 
#undef ADD_FIELDS
    return *this;
}

void food_item::print_item(const FoodItem &item)
{
    using std::cout;
    using std::endl;
    cout << "Id : " << item.pImpl->id << endl << 
    "Name : " << item.pImpl->name << endl <<
    "Protein per gram : " << item.pImpl->proteins_per_gram << endl <<
    "Fat per gram : " << item.pImpl->fats_per_gram << endl <<
    "Carbohydrate per gram : " << item.pImpl->carbohydrates_per_gram << endl <<
    "Calorie per gram : " << item.pImpl->calories_per_gram << endl;
}
