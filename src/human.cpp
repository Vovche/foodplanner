#include "human.h"

using human_stuff::Human;


Human::Human(Sex sex, uint age, double height, double weight, HAC coeff) :
        sex_(sex), age_(age), height_(height), weight_(weight)
{
    auto coeffarr = hbcoeffs[sex == Sex::male? 0:1];
    auto hbactcoeff = hbactivity[static_cast<size_t>(coeff)];
    calculate_norm_ = [coeffarr = coeffarr, hbactcoeff = hbactcoeff]
        (uint age, double height, double weight) {
            return hbactcoeff * (coeffarr[0] +
                coeffarr[1] * weight +
                coeffarr[2] * height -
                coeffarr[3] * age);
        };
}

double Human::get_daily_norm(void)
{
    return calculate_norm_(age_, height_, weight_);
}
